package com.gmail.jd4656.stack;

import com.destroystokyo.paper.event.player.PlayerArmorChangeEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class EventListeners implements Listener {

    @EventHandler
    public void armorChange(PlayerArmorChangeEvent event) {
        Player player = event.getPlayer();
        ItemStack newItem = event.getNewItem();

        if (newItem == null) return;

        if (newItem.getAmount() > 1) {
            ItemStack dropItem = newItem.clone().subtract(1);
            newItem.setAmount(1);

            switch (event.getSlotType()) {
                case HEAD:
                    player.getInventory().setHelmet(newItem);
                    break;
                case CHEST:
                    player.getInventory().setChestplate(newItem);
                    break;
                case LEGS:
                    player.getInventory().setLeggings(newItem);
                    break;
                case FEET:
                    player.getInventory().setBoots(newItem);
                    break;
            }

            player.getWorld().dropItem(player.getLocation().clone().add(0, 1, 0), dropItem);
        }
    }
}
