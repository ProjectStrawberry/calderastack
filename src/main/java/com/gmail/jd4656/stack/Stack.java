package com.gmail.jd4656.stack;

import org.bukkit.plugin.java.JavaPlugin;

public class Stack extends JavaPlugin {
    @Override
    public void onEnable() {

        getServer().getPluginManager().registerEvents(new EventListeners(), this);
        this.getCommand("stack").setExecutor(new CommandStack());
    }
}
