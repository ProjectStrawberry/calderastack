package com.gmail.jd4656.stack;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionType;

import java.util.*;

public class CommandStack implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
         if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }
        if (!sender.hasPermission("stack.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        Player player = (Player) sender;
        PlayerInventory inv = player.getInventory();
        ItemStack[] items = inv.getStorageContents();
        Map<Integer, ItemStack> stackedItems = new LinkedHashMap<>();

        for (ItemStack curItem : items) {
            if (curItem == null) continue;
            if (curItem.getType() == Material.POTION || curItem.getType() == Material.SPLASH_POTION) {
                PotionMeta meta = (PotionMeta) curItem.getItemMeta();
                if (meta.getBasePotionData().getType() != PotionType.WATER && !sender.hasPermission("stack.potions")) continue;
            }
            ItemStack clonedItem = curItem.clone();
            clonedItem.setAmount(1);
            if (!stackedItems.containsKey(clonedItem.hashCode())) {
                stackedItems.put(clonedItem.hashCode(), curItem);
            } else {
                ItemStack newItem = stackedItems.get(clonedItem.hashCode());
                newItem.setAmount(newItem.getAmount() + curItem.getAmount());
                stackedItems.replace(clonedItem.hashCode(), newItem);
            }

            inv.removeItem(curItem);
        }

        for (Map.Entry<Integer, ItemStack> entry : stackedItems.entrySet()) {
            ItemStack curItem = entry.getValue();
            int amount = curItem.getAmount();
            while (amount > 0) {
                ItemStack clonedItem = curItem.clone();
                if (amount >= 64) {
                    clonedItem.setAmount(64);
                    amount -= 64;
                } else {
                    clonedItem.setAmount(amount);
                    amount -= amount;
                }

                inv.addItem(clonedItem);
            }
        }

        sender.sendMessage(ChatColor.GOLD + "Your items have been turned into stacks.");
        return true;
    }
}
